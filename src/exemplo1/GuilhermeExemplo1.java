/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Guilherme
 */
public class GuilhermeExemplo1  {

        public static void main(String [] args){
            int numeroCarros = 20;
            
            System.out.println("Inicio da criacao das threads.");
            
            HashMap tempos = new HashMap();
            
            ExecutorService obj = Executors.newCachedThreadPool();
            
            for (int i = 0; i < numeroCarros; i++)
                obj.execute(new Thread(new GuilhermePrintTasks(tempos, i)));
     
            obj.shutdown();
            
            try {
                obj.awaitTermination(1, TimeUnit.DAYS);
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
            
            float menorTempo = (float) tempos.get(0);
            int vencedor = 0;
            
            for (int i = 1; i < numeroCarros; i++) {
                float t = (float) tempos.get(i);
                if (menorTempo > t) {
                    menorTempo = t;
                    vencedor = i;
                }
            }
            
            System.out.printf("Vencedor: Carro %d, tempo %f ms", vencedor, menorTempo);
        }
        
}
