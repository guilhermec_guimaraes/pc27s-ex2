/**
 * Exemplo1: Programacao com threads
 * Autor:Guilherme
 * Ultima modificacao: 07/08/2017
 */
package exemplo1;

import java.util.HashMap;
import java.util.Random;

/**
 *
 * @author Lucio
 */
public class GuilhermePrintTasks implements Runnable {

    private final HashMap tempos;
    private final static Random generator = new Random();
    private float tempo = 0;
    
    private int ID;

    public GuilhermePrintTasks(HashMap tempos, int ID){
        this.tempos = tempos;
        this.ID = ID;
        //Tempo aleatorio entre 0 e 1 segundo
        
    }
    
    public void run(){
        try{
            for (int i = 0; i < 5; i++) {
                int sleepTime = generator.nextInt(5000); //milissegundos
                Thread.sleep(sleepTime);
                this.tempo += sleepTime;
                System.out.printf("volta %d: carro %d tempo %d ms, total %f\n", i + 1, ID, sleepTime, this.tempo);
                this.tempos.put(this.ID, this.tempo);
                // fgdcgdcg("Carro " + i, i, v):                
            }
        } catch (InterruptedException ex){
            System.out.printf("%s %s\n", ID, "terminou de maneira inesperada.");
        }
       System.out.printf("%s terminou!\n", ID);
    }
       
}
